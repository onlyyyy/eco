
// @ts-ignore
import moment from "moment";

export default class Utils {
	static getFormatNowTime (format?: string) {
		return moment().format(format || "YYYY-MM-DD HH:mm:ss")
	}
}