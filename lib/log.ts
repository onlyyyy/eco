import log4js from 'log4js'
import Store from './store'
import fs from 'fs'
export default class Log {
	
	static init() {
		const LogConfig = Store.getConfig("log")
		const logpath = LogConfig.logpath
		if (LogConfig.cleanAfterStart){
			fs.writeFileSync(logpath, '')
		}
		log4js.configure({
			appenders: {
				'out': { type: 'console' },
				'file': { type: 'file', filename: logpath }
			},
			categories: {
				default: { appenders: ['out'], level: 'INFO' },
				file: { appenders: ['file'], level: 'INFO',  }
			}
		})
	}
	
	static getLogger(name: string) {
		return log4js.getLogger(name)
	}
}