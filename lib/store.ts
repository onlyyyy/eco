// 存储一些变量的类
export default class Store {
  static config: { [key: string]: any } = {}
  static store: { [key: string]: any } = {}

  static set(key: string, value: any) {
    Store.store[key] = value
  }

  static get (key: string) {
    return Store.store[key]
  }

  static setConfig(value: { [key: string]: any }) {
    Store.config = value
  }

  static getConfig (key: string): any {
    return Store.config[key]
  }
}

