import * as sql from '../sqlite/sql'
import MyDb from "../sqlite/db";
import {countProduct} from "../sqlite/sql/products";
import Logger from '../../lib/log'
const log = Logger.getLogger("ProductService")
import {defaultProduct} from '../sqlite/preDefine'

export default class ProductService {
	// 检查商品数 入库
	static async checkProduct() {
		const result = await MyDb.select(sql.products.countProduct)
		const count = result[0].count
		log.info(`目前共${count}个产品`)
		if (count !== defaultProduct.length) {
			log.info("检测到变动，开始重新创建商品")
			await MyDb.runSQL(sql.products.cleanTable)
			for (const product of defaultProduct) {
				const SQL = sql.products.createProduct(product.name, product.key, product.price, product.percent)
				await MyDb.insert(SQL)
				log.info(`创建了商品${product.name}`)
			}
		}
	}
}