// 管理生成的这些用户的行为
import MyDb from "../sqlite/db";
import * as sql from '../sqlite/sql'
import { v4 } from 'uuid'
// @ts-ignore
import randomName from 'node-random-name'
import Log from "../../lib/log";
enum tendency {
	farm = "farm", // 提供收益
	business = "business", // 商人
	leader = "leader", // 带队
	pk = "pk", // pvp
	rich = "rich"
}

const tendencyMap = {
	1: tendency.farm, // 提供收益
	2: tendency.business, // 商人
	3: tendency.leader, // 带队
	4: tendency.pk, // pvp
	5: tendency.rich // 高端用户
}
const log = Log.getLogger('UserService')

export default class UserService {
	
	// 检查用户的数量
	static async checkUser () {
		const result = await MyDb.select(sql.users.countUser)
		const count = result[0].count
		log.info(`目前共${count}个用户`)
		if (count < 1000) {
			for(let i = count; i < 1000; i++) {
				await UserService.createDefaultUser()
			}
			const newRes = await MyDb.select(sql.users.countUser)
			const newCount = newRes[0].count
			log.info('创建用户完毕, 用户数为 ', newCount)
		}

	}
	
	static async createDefaultUser () {
		try {
			const nameArr = randomName().split(' ')
			const random = v4()
			const lastRandom = random.split('-')[0]
			const first = nameArr[0] + lastRandom
			const last = nameArr[1] + lastRandom
			const top5: 1 | 2 | 3 | 4 | 5 = Math.ceil(Math.random() * 5) as unknown as any
			const next5: 1 | 2 | 3 | 4 | 5 = Math.ceil(Math.random() * 5) as unknown as any
			await MyDb.insert(sql.users.createUser(first, tendencyMap[top5]))
			await MyDb.insert(sql.users.createUser(last, tendencyMap[next5]))
		} catch (err) {
			log.info("创建用户出错", err)
		}
	}
}