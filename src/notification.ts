import events from 'events'

class Notification {
	eventEmitter:  events.EventEmitter
	constructor() {
		this.eventEmitter = new events.EventEmitter
	}
	on(eventName:string, listener:(...args: any[]) => void) {
		this.eventEmitter.on(eventName, listener)
	}
	
	// 只执行一次就被移除了
	once(eventName:string, listener:(...args: any[]) => void) {
		this.eventEmitter.once(eventName, listener)
	}
	emit(eventName:string, ...args:any[]) {
		this.eventEmitter.emit(eventName, ...args)
	}
	remove(eventName:string, listener:(...args: any[]) => void) {
		this.eventEmitter.removeListener(eventName, listener)
	}
	
	static self:Notification
	static get instance():Notification {
		if (!this.self) this.self = new Notification
		return this.self
	}
}

export default Notification