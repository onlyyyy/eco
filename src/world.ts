import Notification from './notification'
import Logger from '../lib/log'
import UserService from "./service/userService";
import ProductService from "./service/productService";

const logger = Logger.getLogger("World")

export default class World {
	static init () {
		Notification.instance.once("start", async() => {
			await World.start()
		})
		Notification.instance.once("init", async() => {
			await World.prepare()
		})
	}
	
	static async start () {
		logger.info("开始模拟")
	}
	
	// 准备应该有的数据
	static async prepare() {
		logger.info("初始化必要数据")
		await UserService.checkUser()
		await ProductService.checkProduct()
	}
}