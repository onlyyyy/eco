// sqlite的操作类
import sqlite3, {Database} from 'sqlite3'
import store from '../../lib/store'
import Logger from '../../lib/log'
const log = Logger.getLogger("Sqlite")
export default class Sqlite {
  static dbHandler: Database

  static async connect (filePath?: string) {
    if (!filePath) {
      const dbConfig = store.getConfig('database')
      filePath = dbConfig.dbpath as unknown as string
    }
    Sqlite.dbHandler = new sqlite3.Database(filePath)
  }
  
  static async select (sql: string) {
    return new Promise((resolve, reject) =>{
      Sqlite.dbHandler.all(sql,function(err,row){
        if (err) {
          reject(err)
        }
        resolve(row)
      })
    })
  }

  private static async dbOp (sql: string) {
    return new Promise((resolve, reject) =>{
      try {
        const prepare = Sqlite.dbHandler.prepare(sql)
        prepare.run()
        resolve(log.info(`数据库操作成功 sql = ${sql}`))
      } catch (err) {
        log.info(`数据库操作失败 sql = ${sql}, err = ${err}`)
        reject(err)
      }
    })
  }

  static async  insert (sql:string) {
    return await Sqlite.dbOp(sql)
  }

  static async  delete (sql:string) {
    return await Sqlite.dbOp(sql)
  }

  static async  update (sql:string) {
    return await Sqlite.dbOp(sql)
  }

  // 一般是建表才用这个方法
  static async  runSQL (sql:string) {
    return await Sqlite.dbOp(sql)
  }
}