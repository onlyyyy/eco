// 商品

import Utils from "../../../lib/utils";

export const createTable = "\n" +
	"CREATE TABLE product (\n" +
	"\tid INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
"\tname TEXT(20),\n" +
	"\tkey TEXT(20),\n" +
"\tprice INTEGER,\n" +
"\tpercent INTEGER,\n" +
"\tcreateAt INTEGER,\n" +
"\tupdateAt INTEGER\n" +
");"

export const checkExist = "select 1 from product"

export const cleanTable = "delete from product;vacuum;"

export const countProduct = "select count(1) as count from product"

export function createProduct (name: string, key: string, price: number, percent: number) {
	return `INSERT INTO product (name, key, price, percent, createAt, updateAt) VALUES('${name}', '${key}', '${price}', '${percent}', '${Utils.getFormatNowTime()}', '${Utils.getFormatNowTime()}');`
}

