// 涉及到创建数据库的sql
import moment from 'moment'
import Utils from "../../../lib/utils";

export const createTable = "CREATE TABLE \"user\" (\n" +
	"\tid INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT\n" +
	", name TEXT, tendency TEXT, createAt TEXT, updateAt TEXT);"


export const checkExist = 'select 1 from user'

export const createUser = (name: string, tendency: string) => {
	return `INSERT INTO \"user\"(name, tendency, createAt, updateAt)  VALUES('${name}', '${tendency}',
 '${Utils.getFormatNowTime()}','${Utils.getFormatNowTime()}');`
}

export const countUser = 'select count(1) as count from user'