import * as goods from './products'
import * as users from './users'
import * as bags from './bags'
import * as products from './products'

export {
	goods,
	users,
	bags,
	products
}