// 预设的一些数据，在这个文件修改他们的初始值

export const defaultProduct = [
	{
		name: "金柳露",
		key: "66",
		price: 30000,
		percent: 10
	},
	{
		name: "超级金柳露",
		key: "c66",
		price: 330000,
		percent: 3
	},
	{
		name: "60级武器",
		key: "60Weapon",
		price: 210000,
		percent: 1
	},
	{
		name: "60级防具",
		key: "60Armor",
		price: 180000,
		percent: 1
	},
	{
		name: "70级武器",
		key: "70Weapon",
		price: 170000,
		percent: 1
	},
	{
		name: "70级防具",
		key: "70Armor",
		price: 110000,
		percent: 1
	},
	{
		name: "80级武器",
		key: "80Weapon",
		price: 460000,
		percent: 1
	},
	{
		name: "80级防具",
		key: "80Armor",
		price: 390000,
		percent: 1
	},
	{
		name: "80级附魔宝珠",
		key: "80TreasureBall",
		price: 12000000,
		percent: 0.6
	},
	{
		name: "100级附魔宝珠",
		key: "100TreasureBall",
		price: 14000000,
		percent: 0.5
	},
	{
		name: "110级附魔宝珠",
		key: "110TreasureBall",
		price: 14000000,
		percent: 0.4
	},
	{
		name: "120级附魔宝珠",
		key: "120TreasureBall",
		price: 14000000,
		percent: 0.3
	},
	{
		name: "月华露",
		key: "MoonLight",
		price: 80000,
		percent: 8
	},
	{
		name: "定魂珠",
		key: "SoulCalmingPearl",
		price: 1540000,
		percent: 1
	},
	{
		name: "定魂珠",
		key: "SoulCalmingPearl",
		price: 1540000,
		percent: 1
	},
	{
		name: "定魂珠",
		key: "SoulCalmingPearl",
		price: 1540000,
		percent: 1
	},
	{
		name: "金刚石",
		key: "Diamond",
		price: 1480000,
		percent: 1
	},
	{
		name: "龙鳞",
		key: "DragonScale",
		price: 360000,
		percent: 1.2
	},
	{
		name: "龙鳞",
		key: "DragonScale",
		price: 360000,
		percent: 1.2
	},
	{
		name: "夜光珠",
		key: "LuminousBead",
		price: 960000,
		percent: 1.1
	},
	{
		name: "避水珠",
		key: "LuminousBead",
		price: 960000,
		percent: 1.1
	},
]