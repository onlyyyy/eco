import Sqlite from './sqlite'
import * as sql from './sql'
import Log from '../../lib/log'
const logger = Log.getLogger('DB')
export default class MyDb {

  // 初始化主要是建表
  static async init (filePath?: string) {
    // 先连接数据库
    await Sqlite.connect(filePath)
    await MyDb.createDefaultTable()
  }
  
  static async createDefaultTable() {
    await MyDb.safelyCreateTable(sql.users.checkExist, sql.users.createTable)
    await MyDb.safelyCreateTable(sql.goods.checkExist, sql.goods.createTable)
    await MyDb.safelyCreateTable(sql.bags.checkExist, sql.bags.createTable)
    await MyDb.safelyCreateTable(sql.products.checkExist, sql.products.createTable)
  }
  
  // 先判断表是否存在再建表
  static async safelyCreateTable(existSQL: string, createSQL: string) {
    try {
      await Sqlite.select(existSQL)
    } catch (err) {
      await Sqlite.runSQL(createSQL)
      logger.info(`创建表成功 existSQL=${existSQL}`)
    }
  }
  
  static async select(sql: string): Promise<any> {
    return await Sqlite.select(sql)
  }
  
  static async insert(sql: string): Promise<any> {
    return await Sqlite.insert(sql)
  }
  
  static async update(sql: string): Promise<any> {
    return await Sqlite.update(sql)
  }
  
  static async delete(sql: string): Promise<any> {
    return await Sqlite.delete(sql)
  }
  
  static async runSQL (sql: string): Promise<any> {
    return await Sqlite.runSQL(sql)
  }
}


