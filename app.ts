import ini from 'ini'
import fs from 'fs'
import Store from './lib/store'
import MyDb from './src/sqlite/db'
import World from './src/world'
import Log from './lib/log'
import Notification from "./src/notification";
const logger = Log.getLogger('app')

async function main () {
  getConfig()
  await init()
  bless()
  Notification.instance.emit('init')
  // 初始化完成就开始
  Notification.instance.on('prepared', ()=>{
    Notification.instance.emit('start')
  })
}
async function init() {
  Log.init()
  const dbConfig = Store.getConfig('database')
  if (!fs.existsSync(dbConfig.dbpath)) {
    fs.writeFileSync(dbConfig.dbpath, "")
  }
  await MyDb.init()
  World.init()
}

function getConfig() {
  const filename = './config.ini'
  const config = ini.parse(fs.readFileSync(filename,'utf-8'))
  Store.setConfig(config)
}

function bless() {
  logger.info('ECO启动成功\n' +
    '____________________  ________   \n' +
    '\\_   _____/\\_   ___ \\ \\_____  \\  \n' +
    ' |    __)_ /    \\  \\/  /   |   \\ \n' +
    ' |        \\\\     \\____/    |    \\\n' +
    '/_______  / \\______  /\\_______  /\n' +
    '        \\/         \\/         \\/')
}


main().catch()